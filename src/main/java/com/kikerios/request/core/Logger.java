package com.kikerios.request.core;

import android.util.Log;

public class Logger {

	public static final boolean DEBUG = BuildConfig.DEBUG;
	private static final String ERROR_TAG = "E/KR/";
	private static final String MIN_TAG = "KR/";

	public static void log(String tag, String msg){
		if(true){
			if(tag != null && msg != null){
				Log.d(MIN_TAG + tag, msg);
			}else{
				Log.e(
						ERROR_TAG + tag,
						"[0] " +
								"msg " + msg
				);
			}
		}
	}

	public static void d(String tag, String msg){
		if(true){
			if(tag != null && msg != null){
				Log.d(MIN_TAG + tag, msg);
			}else{
				Log.e(
						ERROR_TAG + tag,
						"[1] " +
								"msg " + msg
				);
			}
		}
	}

	public static void e(String tag, String msg){
		//Show always the errors
		if(true){
			if(tag != null && msg != null){
				Log.e(MIN_TAG + tag, msg);
			}
			else{
				Log.e(
						ERROR_TAG + tag,
						"[2] " +
								"msg " + msg
				);
			}
		}
	}

	public static void e(String tag, String msg, Throwable tr){
		if(true){
			if(tag != null && msg != null && tr != null){
				Log.e(MIN_TAG + tag, msg, tr);
			}else{
				Log.e(
						ERROR_TAG + tag,
						"[3] " +
								"msg " + msg +
								"tr " + tr
				);
			}
		}
	}

	public static void i(String tag, String msg){
		if(true){
			if(tag != null && msg != null){
				Log.i(MIN_TAG + tag, msg);
			}else{
				Log.e(
						ERROR_TAG + tag,
						"[4] " +
								"msg " + msg
				);
			}
		}
	}

	public static void v(String tag, String msg){
		if(true){
			if(tag != null && msg != null){
				Log.v(MIN_TAG + tag, msg);
			}else{
				Log.e(
						ERROR_TAG + tag,
						"[5] " +
								"msg " + msg
				);
			}
		}
	}

	public static void w(String tag, String msg){
		if(true){
			if(tag != null && msg != null){
				Log.w(MIN_TAG + tag, msg);
			}else{
				Log.e(
						ERROR_TAG + tag,
						"[6] " +
								"msg " + msg
				);
			}
		}
	}

}