package com.kikerios.request.core;

import android.os.Handler;
import android.os.Looper;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kikerios on 26/03/16.
 */
public class RetrofitHelper {

    protected static final String TAG = RetrofitHelper.class.getSimpleName();

    public static <T> void runService(final Observable observable, final RetrofitSubscriber<T> subscriber) {

        /**
         * FIX MainThread
         **/
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
            }
        }, 250);

    }

}
