package com.kikerios.request.core;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Connection;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpEngine;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kikerios on 24/03/16.
 **/
public class RetrofitManager {

    public static final String TAG = RetrofitManager.class.getSimpleName();

    /**
     * Creates a retrofit service from an arbitrary class (clazz)
     * @param clazz Java interface of the retrofit service
     * @param endPoint REST endpoint url
     * @param interceptor
     * @param <T>
     * @return retrofit service with defined endpoint
     **/
    public static <T> T createRetrofitService(final Class<T> clazz, final String endPoint, final URL fullUrl, CustomInterceptor interceptor) {

        if(interceptor == null)
            interceptor = new CustomInterceptor();

        interceptor.setEndPoint(fullUrl.toString());

        OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .connectTimeout(10, TimeUnit.MINUTES)
            .addInterceptor(interceptor).build();

        Retrofit.Builder retrofit_builder = new Retrofit.Builder();

        retrofit_builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
        retrofit_builder.addConverterFactory(GsonConverterFactory.create());
        retrofit_builder.client(client);
        retrofit_builder.baseUrl(endPoint);

        Retrofit retrofit = retrofit_builder.build();

        T service = retrofit.create(clazz);

        return service;
    }

    /**
     * CustomInterceptor - extend package okhttp3.logging.HttpLoggingInterceptor;
     **/
    public static class CustomInterceptor implements Interceptor {

        protected String endPoint;
        protected RequestBody requestBody;
        protected ResponseBody responseBody;
        protected Headers requestHeaders;
        protected Headers responseHeaders;
        protected boolean hasRequestBody;
        protected static final Charset UTF8 = Charset.forName("UTF-8");
        private HashMap<String, String> requestHeadersList = new HashMap<>();
        private HashMap<String, String> responseHeadersList = new HashMap<>();
        private String fullBody = null;

        public Response response;
        public Request request;

        public long startNs, tookMs, bufferSize;

        public void setEndPoint(String endPoint) {
            this.endPoint = endPoint;
        }

        public String getEndPoint() {
            return this.endPoint;
        }

        /**
         * @param chain
         * @return
         * @throws IOException
         **/
        @Override
        public Response intercept(Chain chain) throws IOException {

            request = chain.request();

            boolean logBody = true;
            boolean logHeaders = true;

            requestBody = request.body();
            hasRequestBody = requestBody != null;

            Connection connection = chain.connection();
            Protocol protocol = connection != null ? connection.protocol() : Protocol.HTTP_1_1;
            String requestStartMessage = "--> " + request.method() + ' ' + request.url() + ' ' + protocol;

            if (!logHeaders && hasRequestBody) {
                requestStartMessage += " (" + requestBody.contentLength() + "-byte body)";
            }
            logger(requestStartMessage);

            if (logHeaders) {
                if (hasRequestBody) {
                    /**
                     * Request body headers are only present when installed as a network interceptor. Force
                     * them to be included (when available) so there values are known.
                     **/
                    if (requestBody.contentType() != null) {
                        logger("Content-Type: " + requestBody.contentType());
                    }

                    if (requestBody.contentLength() != -1) {
                        logger("Content-Length: " + requestBody.contentLength());
                    }
                }

                requestHeaders = request.headers();
                for (int i = 0, count = requestHeaders.size(); i < count; i++) {
                    String name = requestHeaders.name(i);
                    /**
                     * Skip headers from the request body as they are explicitly logged above.
                     **/
                    if (!"Content-Type".equalsIgnoreCase(name) && !"Content-Length".equalsIgnoreCase(name)) {
                        logger(name + ": " + requestHeaders.value(i));
                    }
                    try {
                        requestHeadersList.put(name, requestHeaders.value(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!logBody || !hasRequestBody) {
                    logger("--> END " + request.method());
                } else if (bodyEncoded(request.headers())) {
                    logger("--> END " + request.method() + " (encoded body omitted)");
                } else {

                    Buffer buffer = new Buffer();
                    requestBody.writeTo(buffer);

                    Charset charset = UTF8;
                    MediaType contentType = requestBody.contentType();
                    if (contentType != null) {
                        charset = contentType.charset(UTF8);
                    }

                    logger("");
                    logger(buffer.readString(charset));
                    logger("--> END " + request.method() + " (" + requestBody.contentLength() + "-byte body)");
                }
            }

            startNs = System.nanoTime();
            response = null;

            /**
             * catch error :S
             **/
            response= chain.proceed(request);

            tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);

            responseBody = response.body();

            long contentLength = responseBody.contentLength();
            String bodySize = contentLength != -1 ? contentLength + "-byte" : "unknown-length";
            logger("<-- " + response.code() + ' ' + response.message() + ' ' + response.request().url() + " (" + tookMs + "ms" + (!logHeaders ? ", " + bodySize + " body" : "") + ')');

            if (logHeaders) {

                responseHeaders = response.headers();
                for (int i = 0, count = responseHeaders.size(); i < count; i++) {
                    try {
                        logger(responseHeaders.name(i).toLowerCase() + ": " + responseHeaders.value(i));
                        responseHeadersList.put(responseHeaders.name(i).toLowerCase(), responseHeaders.value(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!logBody || !HttpEngine.hasBody(response)) {
                    logger("<-- END HTTP");
                } else if (bodyEncoded(response.headers())) {
                    logger("<-- END HTTP (encoded body omitted)");
                } else {
                    BufferedSource source = responseBody.source();
                    source.request(Long.MAX_VALUE); // Buffer the entire body.
                    Buffer buffer = source.buffer();

                    Charset charset = UTF8;
                    MediaType contentType = responseBody.contentType();
                    if (contentType != null) {
                        try {
                            charset = contentType.charset(UTF8);
                        } catch (UnsupportedCharsetException e) {
                            logger("");
                            logger("Couldn't decode the response body; charset is likely malformed.");
                            logger("<-- END HTTP");

                            return response;
                        }
                    }

                    if (contentLength != 0) {
                        fullBody = buffer.clone().readString(charset);
                        logger("--> fullBody");
                        logger(fullBody);
                    }

                    bufferSize = buffer.size();
                    logger("<-- END HTTP (" + bufferSize + "-byte body)");
                }
            }

            return response;
        }

        /**
         * @param log
         **/
        private void logger(String log) {
            Logger.d(TAG + "/" + endPoint, log);
        }

        /**
         * @param headers
         * @return
         **/
        private boolean bodyEncoded(Headers headers) {
            String contentEncoding = headers.get("Content-Encoding");
            return contentEncoding != null && !contentEncoding.equalsIgnoreCase("identity");
        }

        public RequestBody getRequestBody() {
            return requestBody;
        }

        public ResponseBody getResponseBody() {
            return responseBody;
        }

        public Headers getRequestHeaders() {
            return requestHeaders;
        }

        public Headers getResponseHeaders() {
            return responseHeaders;
        }

        public HashMap<String, String> getRequestHeadersList() {
            return requestHeadersList;
        }

        public HashMap<String, String> getResponseHeadersList() {
            return responseHeadersList;
        }

        public String getFullBody() {
            return fullBody;
        }

        public static String parseBody(ResponseBody responseBody){
            Logger.d(TAG, "parseBody " + responseBody);

            if (responseBody != null) {

                try {

                    BufferedSource source = responseBody.source();
                    source.request(Long.MAX_VALUE); // Buffer the entire body.
                    Buffer buffer = source.buffer();

                    Charset charset = UTF8;
                    MediaType contentType = responseBody.contentType();
                    if (contentType != null) {
                        try {
                            charset = contentType.charset(UTF8);
                        } catch (UnsupportedCharsetException e) {
                            e.printStackTrace();
                        }
                    }

                    return buffer.clone().readString(charset);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }
}
