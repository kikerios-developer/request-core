package com.kikerios.request.core;

import android.support.annotation.CallSuper;
import rx.Subscriber;

/**
 * Created by kikerios on 4/04/16.
 */
public class RetrofitSubscriber<T> extends Subscriber<T> {

    private static final String TAG = RetrofitSubscriber.class.getSimpleName();
    private RetrofitManager.CustomInterceptor interceptor;

    public RetrofitSubscriber() {
        Logger.d(TAG, "new RetrofitSubscriber");
        interceptor = new RetrofitManager.CustomInterceptor();
    }

    @CallSuper
    @Override
    public void onCompleted() {
        Logger.d(TAG, "onCompleted " + getInterceptor().getEndPoint());
    }

    @CallSuper
    @Override
    public void onError(Throwable e) {
        Logger.e(TAG, "onError " + getInterceptor().getEndPoint() + " " + e);
        e.printStackTrace();
    }

    @CallSuper
    @Override
    public void onNext(T t) {
        Logger.d(TAG, "onNext " + getInterceptor().getEndPoint() + " " + t);
    }

    public RetrofitManager.CustomInterceptor getInterceptor() {
        return this.interceptor;
    }
}