package com.kikerios.request.core;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * Created by kikerios on 30/08/16.
 */

public class RetrofitUrl {

    protected static final String TAG = RetrofitUrl.class.getSimpleName();

    private String url;
    private URL uri;
    private String base, host, path, query;

    public RetrofitUrl(String url) {

        this.url = url;
        this.uri = parseURL();

        this.base = ( (getURI() != null) ? (getURI().getProtocol() + "://" + getURI().getHost() + getURI().getPath() + "/") : "" );
        this.host = ( (getURI() != null) ? getURI().getProtocol() + "://" + getURI().getHost() : "" );
        this.path = ( (getURI() != null) ? getURI().getPath() : "" );
        this.query = "?" +  ( (getURI() != null && getURI().getQuery() != null) ? getURI().getQuery() : "" );

    }

    /**
     * Prepare URLs - Parse Url
     **/
    private URL parseURL() {
        try {
            return new URL(URLDecoder.decode(url, "UTF-8" ));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isValidUrl() {
        return (getURL() != null && !getURL().trim().isEmpty() && getURI() != null);
    }

    public URL getURI() {
        return this.uri;
    }

    public String getURL() {
        return this.url;
    }

    /**
     * Prepare URLs - Get Base URL
     **/
    public String getBaseURL() {
        return this.base;
    }

    /**
     * Prepare URLs - Get Base HOST URL
     **/
    public String getBaseHostURL() {
        return this.host;
    }

    /**
     * Prepare URLs - Get Base PATH URL
     **/
    public String getBasePathURL() {
        return this.path;
    }

    /**
     * Prepare URLs - Get URL Parameters
     **/
    public String getQueryURL() {
        return this.query;
    }

    /**
     * Prepare URLs - Get URL Parameters
     **/
    public String updateQueryURL(String key, String value) {
        if (key != null && value != null)
            this.query = getQueryURL().replace(key, value);
        return getQueryURL();
    }

}
